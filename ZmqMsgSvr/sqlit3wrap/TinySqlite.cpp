#include "TinySqlite.h"
#include "Include/log.h"
#ifdef LOG_DB
#include <Windows.h>
#endif

//sqlite3* CTinySqlite::m_pDB=NULL;

CTinySqlite::CTinySqlite(void)
{
	m_pDB = 0;
}


CTinySqlite::~CTinySqlite(void)
{
	close();
}

bool CTinySqlite::open( const char* szFile )
{
	/*if (m_pDB && (strcmp(szFile,m_curdbfile.c_str()) == 0))
	{
	return true;
	}*/
	if (!m_pDB)
	{
		close();
		int rc  = sqlite3_open(szFile, &m_pDB);
		if (rc)
		{
#ifdef LOG_DB
			LOG(4)("open db file %s error,error code = %d",szFile,rc);
#endif
			return false;
		}
		m_curdbfile = szFile;
		SetBuyTime(100);
	}
	
	return true;
}

void CTinySqlite::close()
{
	if (m_pDB)
	{
		sqlite3_close(m_pDB);
		m_pDB = 0;
	}
}

int CTinySqlite::exec( const char* szSQL )
{
	if (!m_pDB)
		return DB_ERROR;
	char* szError=0;
#ifdef LOG_DB
	DWORD begcount = GetTickCount();
#endif
	int nRet = sqlite3_exec(m_pDB, szSQL, 0, 0, &szError);
	if (nRet != SQLITE_OK)
	{
#ifdef LOG_DB
		LOG(4)(" exec %s error \r\n msg = %s\r\n",szSQL,szError);
#endif
		sqlite3_free(szError);
		return DB_ERROR;
	}
	else
	{
#ifdef LOG_DB
		LOG(4)("exec %s elapse = %lu",szSQL,GetTickCount() - begcount);
#endif
		return sqlite3_changes(m_pDB);
	}
	
	
}

void CTinySqlite::SetBuyTime( int ms )
{
	if (m_pDB)
	{
		sqlite3_busy_timeout(m_pDB,ms);
	}
}

SQLiteTable* CTinySqlite::getTable(const char* szSQL)
{
	if (!m_pDB)
		 return NULL;
	char* szError=0;
	char** paszResults=0;
	int nRet;
	int nRows(0);
	int nCols(0);
	nRet = sqlite3_get_table(m_pDB, szSQL, &paszResults, &nRows, &nCols, &szError);
	if (nRet == SQLITE_OK)
	{
	
		return new SQLiteTable(paszResults, nRows, nCols);
	}
	else
	{
		sqlite3_free(szError);
		return NULL;
	}
	
	
}
