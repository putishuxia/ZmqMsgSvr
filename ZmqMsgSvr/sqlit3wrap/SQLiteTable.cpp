#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "SQLiteTable.h"
#include "include/sqlite3/sqlite3.h"

SQLiteTable::SQLiteTable(void)
{
	mpaszResults = 0;
	mnRows = 0;
	mnCols = 0;
	mnCurrentRow = 0;
}

SQLiteTable::SQLiteTable( char** paszResults, int nRows, int nCols )
{
	mpaszResults = paszResults;
	mnRows = nRows;
	mnCols = nCols;
	mnCurrentRow = 0;
}

SQLiteTable::SQLiteTable( const SQLiteTable& rTable )
{
	mpaszResults = rTable.mpaszResults;
	// Only one object can own the results
	const_cast<SQLiteTable&>(rTable).mpaszResults = 0;
	mnRows = rTable.mnRows;
	mnCols = rTable.mnCols;
	mnCurrentRow = rTable.mnCurrentRow;
}


SQLiteTable::~SQLiteTable(void)
{
	try
	{
		finalize();
	}
	catch (...)
	{
	}
}



void SQLiteTable::finalize()
{
	if (mpaszResults)
	{
		sqlite3_free_table(mpaszResults);
		mpaszResults = 0;
	}
}


int SQLiteTable::numFields()
{
	checkResults();
	return mnCols;
}


int SQLiteTable::numRows()
{
	checkResults();
	return mnRows;
}


const char* SQLiteTable::fieldValue(int nField)
{
	checkResults();

	if (nField < 0 || nField > mnCols-1)
	{
		return 0;
	}

	int nIndex = (mnCurrentRow*mnCols) + mnCols + nField;
	return mpaszResults[nIndex];
}


const char* SQLiteTable::fieldValue(const char* szField)
{
	checkResults();

	if (szField)
	{
		for (int nField = 0; nField < mnCols; nField++)
		{
			if (strcmp(szField, mpaszResults[nField]) == 0)
			{
				int nIndex = (mnCurrentRow*mnCols) + mnCols + nField;
				return mpaszResults[nIndex];
			}
		}
	}

	return 0;
}


int SQLiteTable::getIntField(int nField, int nNullValue/*=0*/)
{
	if (fieldIsNull(nField))
	{
		return nNullValue;
	}
	else
	{
		return atoi(fieldValue(nField));
	}
}


int SQLiteTable::getIntField(const char* szField, int nNullValue/*=0*/)
{
	if (fieldIsNull(szField))
	{
		return nNullValue;
	}
	else
	{
		return atoi(fieldValue(szField));
	}
}


double SQLiteTable::getFloatField(int nField, double fNullValue/*=0.0*/)
{
	if (fieldIsNull(nField))
	{
		return fNullValue;
	}
	else
	{
		return atof(fieldValue(nField));
	}
}


double SQLiteTable::getFloatField(const char* szField, double fNullValue/*=0.0*/)
{
	if (fieldIsNull(szField))
	{
		return fNullValue;
	}
	else
	{
		return atof(fieldValue(szField));
	}
}


const char* SQLiteTable::getStringField(int nField, const char* szNullValue/*=""*/)
{
	if (fieldIsNull(nField))
	{
		return szNullValue;
	}
	else
	{
		return fieldValue(nField);
	}
}


const char* SQLiteTable::getStringField(const char* szField, const char* szNullValue/*=""*/)
{
	if (fieldIsNull(szField))
	{
		return szNullValue;
	}
	else
	{
		return fieldValue(szField);
	}
}


bool SQLiteTable::fieldIsNull(int nField)
{
	checkResults();
	return (fieldValue(nField) == 0);
}


bool SQLiteTable::fieldIsNull(const char* szField)
{
	checkResults();
	return (fieldValue(szField) == 0);
}


const char* SQLiteTable::fieldName(int nCol)
{
	checkResults();

	if (nCol < 0 || nCol > mnCols-1)
	{
		return 0;
	}

	return mpaszResults[nCol];
}


void SQLiteTable::setRow(int nRow)
{
	checkResults();

	if (nRow < 0 || nRow > mnRows-1)
	{
		return;
	}

	mnCurrentRow = nRow;
}


void SQLiteTable::checkResults()
{
	if (mpaszResults == 0)
	{
		
	}
}

SQLiteTable& SQLiteTable::operator=( const SQLiteTable& rTable )
{
	finalize();
	mpaszResults = rTable.mpaszResults;
	// Only one object can own the results
	const_cast<SQLiteTable&>(rTable).mpaszResults = 0;
	mnRows = rTable.mnRows;
	mnCols = rTable.mnCols;
	mnCurrentRow = rTable.mnCurrentRow;
	return *this;
}
