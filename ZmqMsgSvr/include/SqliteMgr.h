#ifndef		SQLITE_MGR_H
#define		SQLITE_MGR_H
#include<string>
#include "TinySqlite.h"
#include "include/icqtypes.h"
#include <vector>
#include "dateStruct.h"

class SqliteMgr
{
public:
	SqliteMgr();
	void  Init(const std::string dbpath);
	~SqliteMgr(void);
private:
	std::string m_dbpath;
	bool m_bOpen;
	CTinySqlite m_db;
	std::string m_notifyTable;
	std::string m_loginStateTable;
public:
	//db
	int OpenDb();
	int CloseDb();
	
	//talbe
	int CreateAllTalbe();
	int CreatNotifyTable(const std::string& table);
	int CreatLoginStateTable(const std::string& table);
	int DeleteTable(const std::string& strTab);

	//Insert
	int InsertTimeMsgIntoTable(const NotifyMsg &msg);
	int InsertLoginStateIntoTable(const LoginState& loginstate);
	//Update
	int UpdateLoginStateMsg(const LoginState& loginstate);
	int UpdateTimeMsg(const NotifyMsg &msg);
	//Select
	int SelectNotifyMsg(NotifyMsgList &msgList);
	int SelectLoginStateTime(LoginStateList &stateList, uint32 lTime, uint32 rTime);
	
	//Delete
	int DeleteNotifyMsg(uint64 incrementId);
	
	//Update
	int UpdateSqliteMsg(const std::string& strTab, const std::string& condition);
	
};
#endif

