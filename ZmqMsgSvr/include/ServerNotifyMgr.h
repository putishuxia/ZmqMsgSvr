#ifndef SERVER_NOTIFY_MGR_H
#define SERVER_NOTIFY_MGR_H
#include "msgpublic.h"
#include "pulicInclude.h"
#include "SqliteMgr.h"
#include "include/json/json.h"
class ServerNotifyMgr:public CoreThreadHandle
{
public:
	ServerNotifyMgr(void);
	~ServerNotifyMgr(void);
private:
	//发布端初始化(发布订阅模型)
	MsgList m_taskList;
	Mutex m_taskMutex;
	MsgList m_resultList;
	Mutex m_resultMutex;
	ThreadHandle m_handle;
	SqliteMgr m_sqlite;
public:
	int Init();
	int HandleMsg(const ZmqMsg *msg);
	virtual void run(void *mParameter);
	const ZmqMsg * GetOneMstFromTaskList();
	const ZmqMsg * GetOneMstFromResultList();
	void ReleaseZmqMsg(const ZmqMsg * msg);
	int AddOneMsgIntoTaskList(ZmqMsg *msg);
	int AddOneMsgIntoResultList(ZmqMsg *msg);
private:
	
	//接收任务处理
	int PutLoginStateIntoDB(const Json::Value &jsVal);
	int PutNotifyMsgIntoDB(const Json::Value &jsVal);
	//检查超时事件
	int CheckUnLoginNotify(uint32 timeSpan);
	int CheckOnTimerNotify();
};

#endif
