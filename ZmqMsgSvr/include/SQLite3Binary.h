#pragma once

////////////////////////////////////////////////////////////////////////////////
// Prototypes for SQLite functions not included in SQLite DLL, but copied below
// from SQLite encode.c
////////////////////////////////////////////////////////////////////////////////
int sqlite3_encode_binary(const unsigned char *in, int n, unsigned char *out);
int sqlite3_decode_binary(const unsigned char *in, unsigned char *out);

class CppSQLite3Buffer
{
public:

	CppSQLite3Buffer();

	~CppSQLite3Buffer();

	const char* format(const char* szFormat, ...);

	operator const char*() { return mpBuf; }

	void clear();

private:

	char* mpBuf;
};

class CppSQLite3Binary
{
public:

	CppSQLite3Binary();

	~CppSQLite3Binary();

	void setBinary(const unsigned char* pBuf, int nLen);
	void setEncoded(const unsigned char* pBuf);

	const unsigned char* getEncoded();
	const unsigned char* getBinary();

	int getBinaryLength();

	unsigned char* allocBuffer(int nLen);

	void clear();

private:

	unsigned char* mpBuf;
	int mnBinaryLen;
	int mnBufferLen;
	int mnEncodedLen;
	bool mbEncoded;
};