#ifndef CENTER_PROCESS_SERVER_H
#define CENTER_PROCESS_SERVER_H
#include <list>
#include "msgpublic.h"
#include "ServerNotifyMgr.h"
class GameCenterProcess:public CoreThreadHandle
{
private:
	//发布端初始化(发布订阅模型)
	void* m_context;		///创建一个新的环境
	void* m_publisher;	/// 创建一个发布者
	//接收端初始化
	void * m_listen;
	MsgList m_list;
	Mutex m_mutex;
	ThreadHandle m_handle;
private:
	ServerNotifyMgr m_svrNotify;
public:
	GameCenterProcess(void);
	~GameCenterProcess(void);
	virtual void run(void *mParameter);
public:
	int Init();
	int HandleRecveMsg();
	int SendMsg(const ZmqMsg *msg);
private:
	int AddOneMsgIntoList(ZmqMsg *msg, int group);
	const ZmqMsg * GetOneMstFromList();
	void ReleaseZmqMsg(const ZmqMsg* msg);
};
#endif


