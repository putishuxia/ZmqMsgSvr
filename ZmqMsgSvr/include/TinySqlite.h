#pragma once
#pragma comment(lib,"sqlite3")
#include "include/sqlite3/sqlite3.h"
#include "SQLiteTable.h"
#include <string>
using namespace std;
#define LOG_DB
class CTinySqlite
{
public:
	enum
	{
		DB_ERROR = -1,
	};
	CTinySqlite(void);
	~CTinySqlite(void);
private:
	 sqlite3* m_pDB;
	string m_curdbfile;
public:
	bool open(const char* szFile);
	void close();
	void SetBuyTime(int ms);
	sqlite3* GetDB(){return m_pDB;}
	/************************************************************************/
	/*  INSERT, UPDATE, or DELETE         
	/* @return 如果操作成功，返回的SQL操作影响的条数 DB_ERROR是错误
	/************************************************************************/
    int exec(const char* szSQL);
	 SQLiteTable* getTable(const char* szSQL);

};

