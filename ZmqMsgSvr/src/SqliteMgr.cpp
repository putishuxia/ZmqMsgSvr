#include "SqliteMgr.h"
#include "SQLite3Binary.h"
#include <time.h>

SqliteMgr::SqliteMgr()
{
}
void SqliteMgr::Init(const std::string dbpath)
{
	m_dbpath = dbpath;
	m_bOpen = false;
	m_notifyTable = "NotifyTime";
	m_loginStateTable= "LoginState";
}

SqliteMgr::~SqliteMgr(void)
{
}
int SqliteMgr::OpenDb()
{
	if(m_bOpen)
	{
		return 0;
	}
	CTinySqlite db;
	if(m_db.open(m_dbpath.c_str()))
	{
		m_bOpen = true;
		return 0;
	}
	else
	{
		return -1;
	}
}
int SqliteMgr::CloseDb()
{
	if(!m_bOpen)
	{
		return -1;
	}
	m_db.close();
	m_bOpen = false;
}
int SqliteMgr::CreatNotifyTable(const std::string& table)
{
	if(!m_bOpen)
	{
		return -1;
	}
	CppSQLite3Buffer buf;
	buf.format("create table if not exists %s("
		"pid INT NOT NULL,"
		"uin INT64 NOT NULL,"
		"notifyType INT NOT NULL,"
		"endTime INT64 NOT NULL,"
		"startTime INT64  NOT NULL,"
		"buildType INT NOT NULL,"
		"flag INT8 NOT NULL,"
		"incrementId  INTEGER PRIMARY KEY AUTOINCREMENT);"
		,table.c_str());

	return m_db.exec(buf);
}
int SqliteMgr::CreatLoginStateTable(const std::string& table)
{
	if(!m_bOpen)
	{
		return -1;
	}
	CppSQLite3Buffer buf;
	buf.format("create table if not exists %s("
		"pid INT NOT NULL,"
		"uin INT64 NOT NULL,"
		"notifyType INT NOT NULL,"
		"loginTime INT64 NOT NULL);"
		,table.c_str());

	return m_db.exec(buf);
}
int SqliteMgr::InsertTimeMsgIntoTable(const NotifyMsg& msg)
{
	if(!m_bOpen)
	{
		return -1;
	}
	CppSQLite3Buffer sql;
	sql.format("insert into %s("
		"pid,uin,"
		"notifyType,buildId, flag,"
		"startTime,endTime) values("
		"%u,%u,"
		"%d,%d,%d,"
		"%u,%u)",m_notifyTable.c_str(),
		msg.pid, msg.uin, msg.notifyType, msg.buildId, 0, msg.startTime, msg.endTime);  // 保存的时候需要校正needtrans，因为发送出去的信息的needtrans是没有赋值的
	int rst = m_db.exec(sql);
	if (5 == rst)
	{
		m_db.SetBuyTime(100);
		printf("locked\n");
	}
	return rst;
}

int SqliteMgr::InsertLoginStateIntoTable(const LoginState& loginstate)
{
	if(!m_bOpen)
	{
		return -1;
	}
	CppSQLite3Buffer sql;
	sql.format("insert into %s("
		"pid,uin,"
		"notifyType, loginTime) values("
		"%u, %u,"
		"%d, %u)",m_loginStateTable.c_str(),
		loginstate.pid, loginstate.uin, loginstate.notifyType, loginstate.loginTime);  // 保存的时候需要校正needtrans，因为发送出去的信息的needtrans是没有赋值的
	return m_db.exec(sql);
}
int SqliteMgr::UpdateLoginStateMsg(const LoginState& loginstate)
{
	char sql[1024] = {0};
	uint32 curr = time(NULL);
	sprintf(sql,"select  * from %s WHERE pid = %u and uin = %u", m_loginStateTable.c_str(), loginstate.pid, loginstate.uin);
	SQLiteTable* ptabel = m_db.getTable(sql);
	if (ptabel)
	{
		uint32 curr = time(NULL);
		sprintf(sql," SET loginTime = %u , flag = 0 WHERE pid = %u and uin = %u", m_loginStateTable.c_str(), loginstate.loginTime, loginstate.pid, loginstate.uin);
		std::string condition = sql;
		UpdateSqliteMsg(m_loginStateTable, condition);
	}
	else
	{
		InsertLoginStateIntoTable(loginstate);
		delete ptabel;
	}
	return 0;
}
int SqliteMgr::UpdateTimeMsg(const NotifyMsg &msg)
{
	char sql[1024] = {0};
	uint32 curr = time(NULL);
	sprintf(sql,"select  * from %s  WHERE pid=%u and uin = %u and buildId = %u and flag = 0 limit 1",m_notifyTable.c_str(), msg.pid, msg.uin, msg.buildId);
	SQLiteTable* ptabel = m_db.getTable(sql);
	if (ptabel)
	{
		uint32 curr = time(NULL);
		sprintf(sql," SET loginTime = %u WHERE pid = %u and uin = %u and buildId = %u ", m_loginStateTable.c_str(), msg.pid, msg.uin, msg.buildId);
		std::string condition = sql;
		UpdateSqliteMsg(m_loginStateTable, condition);
	}
	else
	{
		InsertTimeMsgIntoTable(msg);
		delete ptabel;
	}
	return 0;
}
int SqliteMgr::SelectNotifyMsg(NotifyMsgList& msgList)
{
	char sql[1024] = {0};
	uint32 curr = time(NULL);
	sprintf(sql,"select  * from %s  WHERE endTime <= %u  and flag = 0 limit 1",m_notifyTable.c_str(), curr);
	SQLiteTable* ptabel = m_db.getTable(sql);
	if (!ptabel)
		return -1;
	for (int nrow =0;nrow < ptabel->numRows();nrow++)
	{
		NotifyMsg msg;
		ptabel->setRow(nrow);
		for (int ncol =0;ncol<ptabel->numFields();ncol++)
		{
			if (strcmp(ptabel->fieldName(ncol),"pid") == 0)
			{
				uint32 pid =0;
				pid = ptabel->getIntField(ncol);
				msg.pid= pid;
			}
			else if (strcmp(ptabel->fieldName(ncol),"uin")==0)
			{
				uint32 uin = 0;
				uin = ptabel->getIntField(ncol);;
				msg.uin = uin;
			}
			else if (strcmp(ptabel->fieldName(ncol),"buildType")==0)
			{
				uint16 buildType = 0;
				buildType = ptabel->getIntField(ncol);
				msg.buildId = buildType;
			}
			else if (strcmp(ptabel->fieldName(ncol),"notifyType")==0)
			{
				uint16 notifyType = 0;
				notifyType = ptabel->getIntField(ncol);
				msg.notifyType = notifyType;
			}
			else if (strcmp(ptabel->fieldName(ncol),"startTime")==0)
			{
				uint32 startTime = 0;
				startTime  = strtoul(ptabel->getStringField(ncol),NULL,10);
				msg.startTime = startTime;
			}
			else if (strcmp(ptabel->fieldName(ncol),"endTime")==0)
			{
				uint32 endTime = 0;
				endTime  = strtoul(ptabel->getStringField(ncol),NULL,10);
				msg.endTime = endTime;
			}
			else if (strcmp(ptabel->fieldName(ncol),"incrementId")==0)
			{
				uint32 incrementId = 0;
				incrementId  = strtoul(ptabel->getStringField(ncol),NULL,10);
				msg.incrementId = incrementId;
			}
		}
		msgList.push_back(msg);
	}
	ptabel->finalize();
	delete ptabel;
	return 0;
}

int SqliteMgr::SelectLoginStateTime(LoginStateList &stateList, uint32 lTime, uint32 rTime)
{
	char sql[1024] = {0};
	uint32 curr = time(NULL);
	sprintf(sql,"select  * from %s WHERE %u - loginTime >= %u and  %u - loginTime < %u", m_loginStateTable.c_str(), curr, lTime, rTime);
	SQLiteTable* ptabel = m_db.getTable(sql);
	if (!ptabel)
		return -1;
	for (int nrow =0;nrow < ptabel->numRows();nrow++)
	{
		LoginState state;
		ptabel->setRow(nrow);
		for (int ncol =0;ncol<ptabel->numFields();ncol++)
		{
			if (strcmp(ptabel->fieldName(ncol),"pid") == 0)
			{
				uint32 pid =0;
				pid = ptabel->getIntField(ncol);
				state.pid= pid;
			}
			else if (strcmp(ptabel->fieldName(ncol),"uin")==0)
			{
				uint32 uin = 0;
				uin = ptabel->getIntField(ncol);;
				state.uin = uin;
			}
			
			else if (strcmp(ptabel->fieldName(ncol),"loginTime")==0)
			{
				uint32 loginTime = 0;
				loginTime  = strtoul(ptabel->getStringField(ncol),NULL,10);
				state.loginTime = loginTime;
			}
		}
		stateList.push_back(state);
	}
	delete ptabel;
	return 0;
}

int SqliteMgr::UpdateSqliteMsg(const std::string& strTab, const std::string& condition)
{
	if(!m_bOpen)
	{
		return -1;
	}
	char sql[256] = {0};
	sprintf_s(sql,256,"UPDATE %s %s ;", strTab.c_str(), condition.c_str());
	return m_db.exec(sql);
};
int SqliteMgr::DeleteNotifyMsg(uint64 incrementId)
{
	if(!m_bOpen)
	{
		return -1;
	}
	char sql[256] = {0};
	sprintf_s(sql,256,"DELETE  FROM %s %s  WHERE incrementId = %llu ;", m_notifyTable.c_str(), incrementId);
	return m_db.exec(sql);
}
int SqliteMgr::DeleteTable(const std::string& strTab)
{
	return 0;
}
int SqliteMgr::CreateAllTalbe()
{
	CreatNotifyTable(m_notifyTable);
	CreatLoginStateTable(m_loginStateTable);
	return 0;
}