#include "ServerNotifyMgr.h"
#include "SQLite3Binary.h"
#include "msgpublic.h"
#include "dateStruct.h"
#include <time.h>
ServerNotifyMgr::ServerNotifyMgr(void)
{
	m_sqlite.Init("gamecenter.db");
	m_sqlite.OpenDb();
	m_sqlite.CreateAllTalbe();
}


ServerNotifyMgr::~ServerNotifyMgr(void)
{
	m_sqlite.CloseDb();
}
void ServerNotifyMgr::run(void *mParameter)
{
	int timeSpan = 10;
	static uint32 lastTime = time(NULL);
	while(1)
	{
		const ZmqMsg *msg = GetOneMstFromTaskList();
		if(msg)
		{
			HandleMsg(msg);
			Sleep(10);
		}
		else
		{
			CheckOnTimerNotify();
			uint32 curr = time(NULL);
			if(curr - lastTime > 10)
			{
				CheckUnLoginNotify(timeSpan);
				CheckOnTimerNotify();
				//timeSpan += 180;
				lastTime = curr;
			}
		}
		Sleep(5);
	}
	return;
}
int ServerNotifyMgr::Init()
{
	m_handle = CoreThread::CreateNewThread(this, NULL);
	if (INVALID_HANDLE_VALUE == m_handle)
	{
		LOG(Log::LOG_LEVEL_ERROR)("CTLConnector::StartUp create thread err = %d\n", m_handle);
		return -1;
	}
	return 0;
}
int ServerNotifyMgr::HandleMsg(const ZmqMsg *msg)
{
	if(!msg)
	{
		return -1;
	}
	if(msg->empty())
	{
		return -1;
	}
	std::string jstr = (*msg)[0].c_str();
	Json::Reader jsR;
	Json::Value	 jsValue;
	if (!jsR.parse(jstr, jsValue, false))
	{
		return -1;
	}
	uint16 cmd = jsValue[JKEY_NOTIFY_TYPE].asUInt();
	switch(cmd)
	{
		case ZmqMsgGroup::CMD_NOTIFY_ONLINE_STATE:
			{
				PutLoginStateIntoDB(jsValue);
			}
			break;
		case ZmqMsgGroup::CMD_NOTIFY_BUILD:
			{
				PutNotifyMsgIntoDB(jsValue);
			}
			break;
		default:
			break;
	}
	return 0;
}
int ServerNotifyMgr::AddOneMsgIntoTaskList(ZmqMsg *msg)
{
	MutexObject obj(m_taskMutex);
	m_taskList.push_back(msg);
	return 0;
}
int ServerNotifyMgr::AddOneMsgIntoResultList(ZmqMsg *msg)
{
	MutexObject obj(m_resultMutex);
	m_resultList.push_back(msg);
	return 0;
}
const ZmqMsg * ServerNotifyMgr::GetOneMstFromTaskList()
{
	const ZmqMsg * msg = NULL;
	MutexObject obj(m_taskMutex);
	int size = m_taskList.size();
	if(size > 0)
	{
		msg = m_taskList.front();
		m_taskList.pop_front();
	}
	return msg;
}
const ZmqMsg * ServerNotifyMgr::GetOneMstFromResultList()
{
	const ZmqMsg * msg = NULL;
	MutexObject obj(m_resultMutex);
	int size = m_resultList.size();
	if(size > 0)
	{
		msg = m_resultList.front();
		m_resultList.pop_front();
	}
	return msg;
}
void ServerNotifyMgr::ReleaseZmqMsg(const ZmqMsg * msg)
{
	delete msg;
}
int ServerNotifyMgr::PutLoginStateIntoDB(const Json::Value &jsVal)
{
	LoginState loginState;
	loginState.pid = jsVal[JKEY_PID].asUInt();
	loginState.uin = jsVal[JKEY_UIN].asUInt();
	loginState.loginTime = jsVal[JKEY_TIMESTAMP].asUInt();

	m_sqlite.InsertLoginStateIntoTable(loginState);
	return 0;
}
int ServerNotifyMgr::PutNotifyMsgIntoDB(const Json::Value &jsVal)
{
	static int i = 0;
	NotifyMsg msg;
	msg.pid = jsVal[JKEY_PID].asUInt();
	msg.uin = jsVal[JKEY_UIN].asUInt();
	msg.startTime = jsVal[JKEY_START_TIME].asUInt();
	msg.endTime = jsVal[JKEY_END_TIME].asUInt();
	msg.buildId = jsVal[JKEY_BUILD_ID].asUInt();
	msg.notifyType = jsVal[JKEY_NOTIFY_TYPE].asUInt();
	return m_sqlite.InsertTimeMsgIntoTable(msg);
}

//
int ServerNotifyMgr::CheckUnLoginNotify(uint32 timeSpan)
{
	LoginStateList stateList;
	m_sqlite.SelectLoginStateTime(stateList, 1000, 5000);
	int size = stateList.size();
	if(size <= 0)
	{
		return 0;
	}
	for (int i = 0; i < size; ++i)
	{
		char sub[SUB_MAX_SIZE] = {0};
		sprintf(sub, "%s%d",TEST_SERVER_PID,stateList[i].pid);
		ZmqMsg * msg = new ZmqMsg;
		msg->push_back(sub);
		msg->push_back(stateList[i].ToJsonString());
		AddOneMsgIntoResultList(msg);
	}
	return 0;
}
int ServerNotifyMgr::CheckOnTimerNotify()
{
	NotifyMsgList notifyList;
	m_sqlite.SelectNotifyMsg(notifyList);
	int size = notifyList.size();
	if(size <= 0)
	{
		return 0;
	}
	for (int i = 0; i < size; ++i)
	{
		if (0 == m_sqlite.DeleteNotifyMsg(notifyList[i].incrementId))
		{
			LOG(0)("%s DeleteNotifyMsg Erro %s\n", __FUNCTION__, notifyList[i].ToJsonString().c_str());
			return -1;
		}
		char sub[SUB_MAX_SIZE] = {0};
		sprintf(sub, "%s%d",TEST_SERVER_PID, notifyList[i].pid);
		ZmqMsg * msg = new ZmqMsg;
		msg->push_back(sub);
		msg->push_back(notifyList[i].ToJsonString());
		AddOneMsgIntoResultList(msg);
	}
	return 0;
}