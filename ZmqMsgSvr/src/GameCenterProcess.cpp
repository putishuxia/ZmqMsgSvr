#include "GameCenterProcess.h"
#include <assert.h>
#include "ServerNotifyMgr.h"
#include "msgpublic.h"
GameCenterProcess::GameCenterProcess(void)
{
}


GameCenterProcess::~GameCenterProcess(void)
{
}

void GameCenterProcess::run(void *mParameter)
{
	//m_svrNotify.Test();
	while(1)
	{
		const ZmqMsg *msg = m_svrNotify.GetOneMstFromResultList();
		if(msg)
		{
			SendMsg(msg);
			ReleaseZmqMsg(msg);
		}
		else
		{
			Sleep(10);
			continue;
		}
		Sleep(100);
	}
	return;
}
int GameCenterProcess::SendMsg(const ZmqMsg *msg)
{
	int size = msg->size();
	if (size > 1)
	{
		int i = 0;
		zmq_setsockopt (m_publisher, ZMQ_SUBSCRIBE, (*msg)[0].c_str(), (*msg)[0].length());
		for (int i = 1; i < size -1; ++i)
		{
			zmq_send (m_publisher, (*msg)[i].c_str(), (*msg)[i].length(), ZMQ_SNDMORE);
		}
		zmq_send (m_publisher, (*msg)[size -1].c_str(), (*msg)[size -1].length(), 0);
	}
	else
	{
		zmq_send (m_publisher, (*msg)[0].c_str(), (*msg)[0].length(), 0);
	}		
	return 0;
}
int GameCenterProcess::Init()
{
	Log::open("zmqmsgsvr.log", 3, true);
	//发布端初始化(发布订阅模型)
	m_context = zmq_ctx_new();///创建一个新的环境
	assert(m_context != NULL);

	int ret = zmq_ctx_set(m_context, ZMQ_MAX_SOCKETS, 5);/// 在该环境中最大只允许一个socket存在
	assert(ret == 0);

	m_publisher = zmq_socket(m_context, ZMQ_PUB);/// 创建一个发布者
	assert(m_publisher != NULL);
	char pAddr[256] = {0};
	sprintf(pAddr,"tcp://*:%d", PUB_SUB_PORT);
	ret = zmq_bind(m_publisher, pAddr);/// 绑定该发布到TCP通信
	assert(ret == 0);
	memset(pAddr, 0, sizeof(pAddr));
	sprintf(pAddr,"tcp://*:%d", SEND_RECV_PORT);
    //创建context，zmq的socket 需要在context上进行创建 

    //创建zmq socket ，socket目前有6中属性 ，这里使用dealer方式
    //具体使用方式请参考zmq官方文档（zmq手册） 
    if((m_listen = zmq_socket(m_context, ZMQ_PULL)) == NULL)
    {
        zmq_ctx_destroy(m_context);
		return -1;
	}
    int iRcvTimeout = 5000;// millsecond
    //设置zmq的接收超时时间为5秒 
    if(zmq_setsockopt(m_listen, ZMQ_RCVTIMEO, &iRcvTimeout, sizeof(iRcvTimeout)) < 0)
    {
        zmq_close(m_listen);
        zmq_ctx_destroy(m_context);
        return -1;
    }
    //绑定地址 tcp://*:????
    //也就是使用tcp协议进行通信，使用网络端口 ????
    if(zmq_bind(m_listen, pAddr) < 0)
    {
        zmq_close(m_listen);
        zmq_ctx_destroy(m_context);
		return -1;
    }
	m_handle = CoreThread::CreateNewThread(this, NULL);
	if (INVALID_HANDLE_VALUE == m_handle)
	{
		LOG(Log::LOG_LEVEL_ERROR)("CTLConnector::StartUp create thread err = %d\n", m_handle);
		return -1;
	}
	if (-1 == m_svrNotify.Init())
	{
		LOG(Log::LOG_LEVEL_ERROR)("m_svrNotify Init Erro\n");
		return -1;
	}
	return 0;
}
int GameCenterProcess::HandleRecveMsg()
{
	static int n = 0;
	while(1)
	{
		bool bFirst = true;
		ZmqMsg *msg = NULL;
		int group = 0;
		while (1) 
		{
			char msgbuf[MSG_MAX_SIZE] = {0};
			int64_t more = 0;
			// 处理所有的消息帧
			int rtn = zmq_recv(m_listen, msgbuf, sizeof(msgbuf), 0);
			if(rtn < 0)
			{
				break;
			}
			msgbuf[rtn] = 0;
			size_t more_size = sizeof (more);
			zmq_getsockopt (m_listen, ZMQ_RCVMORE, &more, &more_size);
			LOG(1)("received message : %s\n", msgbuf);
			if ((!more && bFirst))
			{
				break;
			}
			if(!msg)
			{
				msg = new ZmqMsg;
			}
			if (bFirst)
			{
				//zmq_setsockopt (m_listen, ZMQ_SUBSCRIBE, msgbuf, strlen(msgbuf));
				group = atoi(msgbuf);
				bFirst = false;
			}	
			else
			{
				msg->push_back(msgbuf);
			}
			memset(msgbuf, 0, sizeof(msgbuf));	
			if(!more && !bFirst)
			{
				break;
			}
		}
		if(msg)
		{
			if(msg->size() >= 1)
			{
				printf("==========AddOneMsgIntoList %d========\n", ++n);
				AddOneMsgIntoList(msg, group);
			}
			else
			{
				delete msg;
			}
		}		
	}
	return 0;
}
int GameCenterProcess::AddOneMsgIntoList(ZmqMsg *msg, int group)
{
	if (0 == group)
	{
		return 0;
	}
	switch(group)
	{
	case ZmqMsgGroup::CMD_NOTIFY_BUILD:
		{
			m_svrNotify.AddOneMsgIntoTaskList(msg);
		}
		break;
	case ZmqMsgGroup::CMD_CHAT:
		{
			MutexObject obj(m_mutex);
			m_list.push_back(msg);
			return 0;
		}
		break;
	case  ZmqMsgGroup::CMD_NOTIFY_ONLINE_STATE:
		{
			m_svrNotify.AddOneMsgIntoTaskList(msg);
		}
		break;
	default:
		break;
	}
	return 0;
}
const ZmqMsg * GameCenterProcess::GetOneMstFromList()
{
	MutexObject obj(m_mutex);
	const ZmqMsg * msg = NULL;
	if(m_list.size() > 0)
	{
		msg = m_list.front();
		m_list.pop_front();
	}
	return msg;
}
void GameCenterProcess::ReleaseZmqMsg(const ZmqMsg* msg)
{
	delete msg;
}